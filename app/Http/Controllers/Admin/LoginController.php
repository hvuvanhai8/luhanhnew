<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    //
    public function getLogin(){
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return view('admin.dashborad.dashborad');
        }
        else{
            return redirect()->back()->with(['error'=>'Email khoặc mật khẩu không đúng ']);
            return view('admin.login');
        }

        

    }

    public function getLogout(Request $request)
    {
        Auth::logout();
        return view('admin.login');
    }
}
