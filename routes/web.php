<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('foo', function () {
    return 'Hello World';
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Admin\LoginController@getLogin')->name('admin.login');
    Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');
    Route::get('/getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');

    
});

